/*
** common.h for myftp in /home/gravie_j/projets/PSU_2013_myftp/common
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Sat Apr 12 16:12:41 2014 Jean Gravier
** Last update Fri Apr 25 14:49:41 2014 Jean Gravier
*/

#ifndef COMMON_H_
# define COMMON_H_
# define BUFFER_SIZE 4096
# define NAME_SIZE 255
# define RESET   "\033[0m"
# define BLACK   "\033[1m\033[30m"
# define RED     "\033[1m\033[31m"
# define GREEN   "\033[1m\033[32m"
# define YELLOW  "\033[1m\033[33m"
# define BLUE    "\033[1m\033[34m"
# define MAGENTA "\033[1m\033[35m"
# define CYAN    "\033[1m\033[36m"
# define WHITE   "\033[1m\033[37m"

typedef struct		s_file
{
  char			name[BUFFER_SIZE];
  size_t		size;
  int			failed;
}			t_file;

typedef struct		s_channel
{
  int			id;
  char			name[NAME_SIZE];
  struct s_channel	*next;
}			t_channel;

typedef struct		s_client
{
  int			id;
  int			fd;
  char			name[NAME_SIZE];
  int			channel_id;
  struct s_client	*next;
}			t_client;

#endif /* !COMMON_H_ */
