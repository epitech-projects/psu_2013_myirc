//
// Window.cpp for irc in /home/gravie_j/projets/PSU_2013_myirc/client
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Fri Apr 18 11:58:12 2014 Jean Gravier
// Last update Sun Apr 27 17:56:40 2014 Jean Gravier
//

#include "Window.hh"
#include <QTimer>

Window::Window()
{
  _container = new QWidget(this);
  _mainLayout = new QGridLayout;
  _chatText = new QTextEdit;
  _chanText = new QTextEdit;
  _messageText = new QLineEdit;
  _sendButton = new QPushButton("Send");

  setFixedSize(800, 600);
  _chatText->setReadOnly(true);
  _chatText->setText("chat");
  _mainLayout->addWidget(_chatText, 0, 0, 1, 3);
  _mainLayout->addWidget(_chanText, 0, 4);
  _mainLayout->addWidget(_messageText, 1, 0, 1, 3);
  _mainLayout->addWidget(_sendButton, 1, 4);
  _container->setLayout(_mainLayout);
  setCentralWidget(_container);
  QTimer::singleShot(5000, this, SLOT(test()));
}

Window::~Window()
{

}

void		Window::test()
{
  delete _sendButton;
}
