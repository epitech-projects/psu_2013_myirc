//
// Window.hh for irc in /home/gravie_j/projets/PSU_2013_myirc/client
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Fri Apr 18 11:59:10 2014 Jean Gravier
// Last update Sun Apr 27 17:52:12 2014 Jean Gravier
//

#ifndef WINDOW_HH_
# define WINDOW_HH_
# include <QMainWindow>
# include <QWidget>
# include <QTextEdit>
# include <QLineEdit>
# include <QGridLayout>
# include <QPushButton>

class			Window: public QMainWindow
{

  Q_OBJECT

public:
  Window();
  virtual ~Window();

public slots:
  void	test();

private:
  QWidget		*_container;
  QGridLayout		*_mainLayout;
  QTextEdit		*_chatText;
  QTextEdit		*_chanText;
  QLineEdit		*_messageText;
  QPushButton		*_sendButton;
};

#endif /* !WINDOW_HH_ */
