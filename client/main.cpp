//
// main.cpp for myirc in /home/gravie_j/projets/PSU_2013_myirc/client
//
// Made by Jean Gravier
// Login   gravie_j<gravie_j@epitech.net>
//
// Started on  Fri Apr 18 13:23:16 2014 Jean Gravier
// Last update Fri Apr 18 15:24:29 2014 Jean Gravier
//

#include <QApplication>
#include "Window.hh"

int		main(int argc, char **argv)
{
  QApplication	app(argc, argv);
  Window	win;

  win.show();
  app.exec();
  return (0);
}
