##
## Makefile for irc in /home/gravie_j/projets/PSU_2013_myirc
##
## Made by Jean Gravier
## Login   gravie_j<gravie_j@epitech.net>
##
## Started on  Sun Apr 27 23:27:58 2014 Jean Gravier
## Last update Sun Apr 27 23:29:49 2014 Jean Gravier
##

CLIENT	=	client/
SERVEUR	=	serveur/

all:
	make -C $(CLIENT)
	make -C $(SERVEUR)

clean:
	make -C $(CLIENT) clean
	make -C $(SERVEUR) clean

fclean:
	make -C $(CLIENT) fclean
	make -C $(SERVEUR) fclean

re:
	make -C $(CLIENT) re
	make -C $(SERVEUR) re
