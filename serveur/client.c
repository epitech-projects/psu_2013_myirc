/*
** client.c for client in /home/gravie_j/projets/PSU_2013_myirc/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Fri Apr 25 14:43:17 2014 Jean Gravier
** Last update Fri Apr 25 15:01:52 2014 Jean Gravier
*/

#include "serveur.h"
#include "common.h"

int		get_new_client_id(t_client *first)
{
  int		id;
  t_client	*tmp;

  id = 0;
  tmp = first;
  while (tmp != NULL)
    {
      id = tmp->id;
      tmp = tmp->next;
    }
  return (id);
}

void		create_client(t_client *first, int fd)
{
  t_client	*tmp;
  int		id;

  tmp = first;
  id = get_new_client_id(tmp);
  if (tmp == NULL)
    {
      if ((tmp = malloc(sizeof(t_client))) == NULL)
	exit(1);
    }
  else
    {
      while (tmp->next != NULL)
	tmp = tmp->next;
      if ((tmp->next = malloc(sizeof(t_client))) == NULL)
	exit(1);
      tmp = tmp->next;
    }
  tmp->id = id;
  tmp->fd = fd;
  memset(tmp->name, 0, NAME_SIZE);
  strncpy(tmp->name, "Mingebag", 8);
  tmp->channel_id = 0;
  tmp->next = NULL;
}
