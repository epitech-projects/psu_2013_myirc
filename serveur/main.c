/*
** main.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 13:22:41 2014 Jean Gravier
** Last update Wed Apr 23 17:06:04 2014 Jean Gravier
*/

#include <string.h>
#include <stdio.h>
#include <signal.h>
#include "serveur.h"

int	main(int argc, char **argv)
{
  if (argc > 1)
    create_connection(argv[1]);
  else
    printf("Usage: ./serveur port\n");
  return (0);
}
