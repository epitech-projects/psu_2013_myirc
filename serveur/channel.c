/*
** channel.c for channel in /home/gravie_j/projets/PSU_2013_myirc/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Fri Apr 25 14:43:11 2014 Jean Gravier
** Last update Fri Apr 25 15:01:22 2014 Jean Gravier
*/

#include "serveur.h"
#include "common.h"

int		get_new_channel_id(t_channel *first)
{
  int		id;
  t_channel	*tmp;

  id = 0;
  tmp = first;
  while (tmp != NULL)
    {
      id = tmp->id;
      tmp = tmp->next;
    }
  return (id);
}

void		create_channel(t_channel *first, char *name)
{
  t_channel	*tmp;
  int		id;

  tmp = first;
  id = get_new_channel_id(tmp);
  if (tmp == NULL)
    {
      if ((tmp = malloc(sizeof(t_channel))) == NULL)
	exit(1);
    }
  else
    {
      while (tmp->next != NULL)
	tmp = tmp->next;
      if ((tmp->next = malloc(sizeof(t_channel))) == NULL)
	exit(1);
      tmp = tmp->next;
    }
  tmp->id = id;
  memset(tmp->name, 0, NAME_SIZE);
  strncpy(tmp->name, name, strlen(name));
  puts(tmp->name);
  tmp->next = NULL;
}
