/*
** serveur.h for myftp in /home/gravie_j/projets/PSU_2013_myftp/serveur
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 13:29:48 2014 Jean Gravier
** Last update Thu Apr 24 19:06:53 2014 Jean Gravier
*/

#ifndef SERVEUR_H_
# define SERVEUR_H_

# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include "common.h"

typedef struct	s_server
{
  int		sock;
  int		port;
  fd_set	readfs;
  t_client	*first_client;
  t_channel	*first_channel;
}		t_server;

int	create_connection(char *);
int	prompt_client(int);
int	put_error(char *);
void	create_client(t_client *, int);
void	create_channel(t_channel *, char *);

#endif /* !SERVEUR_H_ */
