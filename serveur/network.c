/*
** network.c for myftp in /home/gravie_j/projets/PSU_2013_myftp/client
**
** Made by Jean Gravier
** Login   gravie_j<gravie_j@epitech.net>
**
** Started on  Tue Apr  8 15:56:37 2014 Jean Gravier
** Last update Thu Apr 24 19:07:24 2014 Jean Gravier
*/

#include <sys/socket.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "common.h"
#include "serveur.h"

int			highest(t_server *server)
{
  t_client		*tmp;
  int			n;

  n = server->sock;
  if (server->first_client != NULL)
    {
      while (tmp != NULL)
	{
	  if (tmp->fd > n)
	    n = tmp->fd;
	  tmp = tmp->next;
	}
    }
  return (n);
}

void			reset_fs(t_server *server)
{
  FD_ZERO(&server->readfs);
  FD_SET(server->sock, &server->readfs);
}

int			wait_client(t_server *server)
{
  int			client_fd;
  socklen_t		in_size;
  struct sockaddr_in	client_in;

  while (1)
    {
      reset_fs(server);
      if (select(highest(server) + 1, &server->readfs, NULL, NULL, NULL) == -1)
	return (put_error("select"));
      if (FD_ISSET(server->sock, &server->readfs))
	{
	  if ((client_fd = accept(server->sock,
				  (struct sockaddr *)&client_in, &in_size)) == -1)
	    return (put_error("accept"));
	  create_client(server->first_client, client_fd);
	  printf("Client %s connected !\n", inet_ntoa(client_in.sin_addr));
	}
    }
  return (EXIT_SUCCESS);
}

int			bind_name(t_server *server)
{
  struct sockaddr_in	in;

  in.sin_family = AF_INET;
  in.sin_port = htons(server->port);
  in.sin_addr.s_addr = INADDR_ANY;
  if (bind(server->sock, (struct sockaddr *)&in, sizeof(in)) == -1)
    return (put_error("bind"));
  return (EXIT_SUCCESS);
}

int			create_connection(char *port)
{
  struct protoent	*pe;
  t_server		server;

  server.port = atoi(port);
  server.first_client = NULL;
  server.first_channel = NULL;
  if ((pe = getprotobyname("TCP")) == NULL)
    return (EXIT_FAILURE);
  if ((server.sock = socket(AF_INET, SOCK_STREAM, pe->p_proto)) == -1)
    return (put_error("socket"));
  if (bind_name(&server) == EXIT_FAILURE)
    return (EXIT_FAILURE);
  if (listen(server.sock, 50) == -1)
    return (put_error("listen"));
  create_channel(server.first_channel, "main");
  if (wait_client(&server) == EXIT_FAILURE)
    return (EXIT_FAILURE);
  if (close(server.sock) == -1)
    return (EXIT_FAILURE);
  return (EXIT_SUCCESS);
}
